package gosig

import (
	"errors"
	"fmt"
	"reflect"
	"sort"
	"sync"
)

type GosigError struct {
	err  error
	code int
}

const (
	maxUint = ^uint(0)
	maxInt  = int(maxUint >> 1)
	minInt  = -maxInt - 1
)

type SlotHandler int
type Combiner func([][]interface{}) interface{}

func LastValueCombiner(results [][]interface{}) interface{} {
	l := len(results)
	if l == 0 {
		return nil
	}
	return results[len(results)-1]
}

func AggregateCombiner(results [][]interface{}) interface{} {
	return results
}

type slotMeta struct {
	slot       interface{}
	group      int
	handler    SlotHandler
	blockCount int
}

type Signal struct {
	slotType   reflect.Type
	slots      []*slotMeta
	endHandler SlotHandler
	combiner   Combiner

	mutex sync.Mutex
}

func NewSignal(slotProto interface{}) (*Signal, error) {
	return NewSignalWithCombiner(slotProto, LastValueCombiner)
}

func NewSignalWithCombiner(slotProto interface{}, combiner Combiner) (*Signal, error) {
	slotType := reflect.TypeOf(slotProto)
	if slotType.Kind() != reflect.Func {
		msg := fmt.Sprintf("want function to define slot type but got type \"%s\"", slotType)
		return nil, errors.New(msg)
	}
	signal := &Signal{
		slotType: slotType,
		combiner: combiner}
	return signal, nil
}

func (self *Signal) SetCombiner(combiner Combiner) {
	self.combiner = combiner
}

func (self *Signal) newHandler() (handler SlotHandler) {
	handler = self.endHandler
	self.endHandler++
	return
}

func (self *Signal) checkSlotType(slot interface{}) error {
	slotType := reflect.TypeOf(slot)
	if slotType != self.slotType {
		msg := fmt.Sprintf("want slot type \"%s\" but got type \"%s\"", self.slotType, slotType)
		return errors.New(msg)
	}
	return nil
}

func (self *Signal) Connect(slot interface{}) (*Connection, error) {
	err := self.checkSlotType(slot)
	if err != nil {
		return nil, err
	}

	self.mutex.Lock()
	defer self.mutex.Unlock()
	handler := self.newHandler()
	group := maxInt
	meta := &slotMeta{slot, group, handler, 0}
	self.slots = append(self.slots, meta)
	return newConnection(self, handler), nil
}

func (self *Signal) ConnectAtFront(slot interface{}) (*Connection, error) {
	err := self.checkSlotType(slot)
	if err != nil {
		return nil, err
	}

	self.mutex.Lock()
	defer self.mutex.Unlock()
	handler := self.newHandler()
	group := minInt
	meta := &slotMeta{slot, group, handler, 0}
	self.slots = append([]*slotMeta{meta}, self.slots...)
	return newConnection(self, handler), nil
}

func (self *Signal) ConnectWithGroup(group int, slot interface{}) (*Connection, error) {
	err := self.checkSlotType(slot)
	if err != nil {
		return nil, err
	}

	self.mutex.Lock()
	defer self.mutex.Unlock()
	handler := self.newHandler()
	i := sort.Search(len(self.slots), func(i int) bool {
		return self.slots[i].group > group
	})
	meta := &slotMeta{slot, group, handler, 0}
	self.slots = append(self.slots[:i], append([]*slotMeta{meta}, self.slots[i:]...)...)
	return newConnection(self, handler), nil
}

func (self *Signal) checkArgsType(args ...interface{}) error {
	if len(args) != self.slotType.NumIn() {
		msg := fmt.Sprintf("want %d arguments but got %d", self.slotType.NumIn(), len(args))
		return errors.New(msg)
	}
	for idx, arg := range args {
		argType := reflect.TypeOf(arg)
		slotArgType := self.slotType.In(idx)
		if argType != slotArgType {
			msg := fmt.Sprintf("want type \"%s\" in argument %d but got type \"%s\"", slotArgType, idx, argType)
			return errors.New(msg)
		}
	}
	return nil
}

func getArgsValues(args ...interface{}) (slice []reflect.Value) {
	slice = make([]reflect.Value, len(args), len(args))
	for idx, arg := range args {
		slice[idx] = reflect.ValueOf(arg)
	}
	return
}

func valuesToInterfaces(values []reflect.Value) (slice []interface{}) {
	slice = make([]interface{}, len(values))
	for idx, value := range values {
		slice[idx] = value.Interface()
	}
	return
}

func (self *Signal) Emit(args ...interface{}) (interface{}, error) {
	err := self.checkArgsType(args...)
	if err != nil {
		return nil, err
	}

	argValues := getArgsValues(args...)

	self.mutex.Lock()
	slots := make([]slotMeta, len(self.slots))
	for idx, meta := range self.slots {
		slots[idx] = *meta
	}
	combiner := self.combiner
	self.mutex.Unlock()

	results := make([][]interface{}, len(self.slots))
	for idx, meta := range slots {
		if meta.blockCount == 0 {
			slotValue := reflect.ValueOf(meta.slot)
			values := slotValue.Call(argValues)
			results[idx] = valuesToInterfaces(values)
		}
	}
	return combiner(results), nil
}

func (self *Signal) Disconnect(handler SlotHandler) error {
	self.mutex.Lock()
	defer self.mutex.Unlock()
	for i, meta := range self.slots {
		if meta.handler == handler {
			self.slots = append(self.slots[:i], self.slots[i+1:]...)
			return nil
		}
	}
	return errors.New("Not found slot")
}

func (self *Signal) Block(handler SlotHandler) error {
	self.mutex.Lock()
	defer self.mutex.Unlock()
	for _, meta := range self.slots {
		if meta.handler == handler {
			meta.blockCount++
			return nil
		}
	}
	return errors.New("Not found slot")
}

func (self *Signal) Unblock(handler SlotHandler) error {
	self.mutex.Lock()
	defer self.mutex.Unlock()
	for _, meta := range self.slots {
		if meta.handler == handler {
			meta.blockCount--
			if meta.blockCount < 0 {
				panic("block count < 0")
				return nil
			}
		}
	}
	return errors.New("Not found slot")
}
