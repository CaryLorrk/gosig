package example

import (
	"fmt"
	"github.com/carylorrk/gosig"
)

func shortLived() {
	fmt.Println("I'm alive!")
}

func ExampleScopedConnections() {
	sig, _ := gosig.NewSignal(func() {})
	func() {
		conn, _ := sig.Connect(shortLived)
		defer conn.Disconnect()
		sig.Emit()
	}()
	sig.Emit()

	// Output:
	// I'm alive!
}
