package example

import (
	"encoding/hex"
	"fmt"
	"github.com/carylorrk/gosig"
)

/*********************************************************************
 *                             Document                              *
 *********************************************************************/

type Document struct {
	signal *gosig.Signal
	text   string
}

func NewDocument() *Document {
	signal, _ := gosig.NewSignal(func() {})
	return &Document{signal, ""}
}

func (self *Document) Connect(slot interface{}) (*gosig.Connection, error) {
	return self.signal.Connect(slot)
}

func (self *Document) Append(s string) {
	self.text = self.text + s
	self.signal.Emit()
}

func (self *Document) GetText() string {
	return self.text
}

/*********************************************************************
 *                             TextView                              *
 *********************************************************************/

type TextView struct {
	doc  *Document
	conn *gosig.Connection
}

func NewTextView(doc *Document) *TextView {
	textView := &TextView{doc, nil}
	textView.conn, _ = doc.Connect(textView.refresh)
	return textView
}

func (self *TextView) Destroy() {
	self.conn.Disconnect()
}

func (self *TextView) refresh() {
	fmt.Println("TextView:", self.doc.GetText())
}

/*********************************************************************
 *                              HexView                              *
 *********************************************************************/

type HexView struct {
	doc  *Document
	conn *gosig.Connection
}

func NewHexView(doc *Document) *HexView {
	hexView := &HexView{doc, nil}
	hexView.conn, _ = doc.Connect(hexView.refresh)
	return hexView
}

func (self *HexView) Destroy() {
	self.conn.Disconnect()
}

func (self *HexView) refresh() {
	fmt.Println("HexView:", hex.EncodeToString([]byte(self.doc.GetText())))
}

/*********************************************************************
 *                               Main                                *
 *********************************************************************/

func ExampleDocmumentView() {
	doc := NewDocument()
	textView := NewTextView(doc)
	_ = textView
	hexView := NewHexView(doc)
	_ = hexView

	doc.Append("Hello World!")
	// Output:
	// TextView: Hello World!
	// HexView: 48656c6c6f20576f726c6421
}
