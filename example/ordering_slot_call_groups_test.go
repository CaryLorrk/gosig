package example

import (
	"fmt"
	"github.com/carylorrk/gosig"
)

func goodMorning() {
	fmt.Println("... and good morning!")
}

func ExampleOrderingSlotCallGroups() {
	sig, _ := gosig.NewSignal(func() {})
	sig.Connect(goodMorning)
	sig.ConnectWithGroup(1, world)
	sig.ConnectWithGroup(0, hello)
	sig.Emit()

	// Output:
	// Hello, World!
	// ... and good morning!
}
