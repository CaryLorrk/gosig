package example

import (
	"github.com/carylorrk/gosig"
)

func ExampleBlockingSlots() {
	sig, _ := gosig.NewSignal(func() {})
	conn, _ := sig.Connect(helloWorld)
	sig.Emit() // Prints "Hello, World!"
	func() {
		conn.Block()
		defer conn.Unblock()
		sig.Emit() // No output: the slot is blocked
	}()
	sig.Emit() // Prints "Hello, World!"
	// Output:
	// Hello, World!
	// Hello, World!
}
