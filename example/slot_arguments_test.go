package example

import (
	"fmt"
	"github.com/carylorrk/gosig"
)

func printArgs(x, y float64) {
	fmt.Println("The arguments are", x, "and", y)
}

func printSum(x, y float64) {
	fmt.Println("The sum is", x+y)
}

func printProduct(x, y float64) {
	fmt.Println("The product is", x*y)
}

func printDifference(x, y float64) {
	fmt.Println("The difference is", x-y)
}

func printQuotient(x, y float64) {
	fmt.Println("The quotient is", x/y)
}

func ExampleSlotArguments() {
	sig, _ := gosig.NewSignal(func(x, y float64) {})
	sig.Connect(printArgs)
	sig.Connect(printSum)
	sig.Connect(printProduct)
	sig.Connect(printDifference)
	sig.Connect(printQuotient)

	sig.Emit(5., 3.)
	// Output:
	// The arguments are 5 and 3
	// The sum is 8
	// The product is 15
	// The difference is 2
	// The quotient is 1.6666666666666667

}
