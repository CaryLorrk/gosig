package example

import (
	"fmt"
	"github.com/carylorrk/gosig"
)

func product(x, y float64) float64 {
	return x * y
}

func quotient(x, y float64) float64 {
	return x / y
}

func sum(x, y float64) float64 {
	return x + y
}

func difference(x, y float64) float64 {
	return x - y
}

func MaximumCombiner(results [][]interface{}) interface{} {
	var max float64
	if len(results) == 0 {
		return max
	}

	max = results[0][0].(float64)
	for _, res := range results {
		num := res[0].(float64)
		if num > max {
			max = num
		}
	}
	return max
}

func ExampleSignalReturnValuesDefault() {
	sig, _ := gosig.NewSignal(func(x, y float64) float64 { return 0 })
	sig.Connect(product)
	sig.Connect(quotient)
	sig.Connect(sum)
	sig.Connect(difference)

	// The default combiner returns a []interface{} containing the return
	// values of the last slot in the slot list, in this case the
	// difference function.
	res, _ := sig.Emit(5., 3.)
	fmt.Println(res.([]interface{})[0])

	sig.SetCombiner(MaximumCombiner)
	// Outputs the maximum value returned by the connected slots, in this case
	// 15 from the product function.
	resMax, _ := sig.Emit(5., 3.)
	fmt.Println(resMax)

	// Output:
	// 2
	// 15
}
