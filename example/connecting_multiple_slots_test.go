package example

import (
	"fmt"
	"github.com/carylorrk/gosig"
)

func hello() {
	fmt.Print("Hello")
}

func world() {
	fmt.Println(", World!")
}

func ExampleConnectingMultipleSlots() {
	sig, _ := gosig.NewSignal(func() {})
	sig.Connect(hello)
	sig.Connect(world)
	sig.Emit()

	// Output:
	// Hello, World!
}
