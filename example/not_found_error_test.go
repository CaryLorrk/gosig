package example

import (
	"fmt"
	"github.com/carylorrk/gosig"
)

func ExampleNotFoundErrorTest() {
	sig, _ := gosig.NewSignal(func() {})
	conn, _ := sig.Connect(helloWorld)

	err := conn.Disconnect()
	if err != nil {
		fmt.Println(err)
	}

	err = conn.Disconnect()
	if err != nil {
		fmt.Println(err)
	}

	err = conn.Block()
	if err != nil {
		fmt.Println(err)
	}

	err = conn.Unblock()
	if err != nil {
		fmt.Println(err)
	}

	// Output:
	//Not found slot
	//Not found slot
	//Not found slot
}
