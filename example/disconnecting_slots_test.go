package example

import (
	"github.com/carylorrk/gosig"
)

func ExampleDisconnectingSlots() {
	sig, _ := gosig.NewSignal(func() {})
	conn, _ := sig.Connect(helloWorld)
	sig.Emit() // Prints "Hello, World!"

	conn.Disconnect()
	sig.Emit() // Does nothing: there are no connected slots

	// Output:
	// Hello, World!
}
