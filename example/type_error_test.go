package example

import (
	"fmt"
	"github.com/carylorrk/gosig"
)

func rightFunc(x int) {
	fmt.Printf("rightFunc(%d)\n", x)
}

func wrongFunc(x float64) {
	fmt.Printf("wrongFunc(%f)\n", x)
}

func ExampleTypeError() {
	sig, err := gosig.NewSignal(1)
	if err != nil {
		fmt.Println(1, err)
	}

	sig, err = gosig.NewSignal(func(int) {})
	if err != nil {
		fmt.Println(2, err)
	}

	_, err = sig.Connect(wrongFunc)
	if err != nil {
		fmt.Println(3, err)
	}

	_, err = sig.Connect(rightFunc)
	if err != nil {
		fmt.Println(4, err)
	}

	_, err = sig.Emit()
	if err != nil {
		fmt.Println(5, err)
	}

	_, err = sig.Emit("wrong")
	if err != nil {
		fmt.Println(6, err)
	}

	_, err = sig.Emit(1)
	if err != nil {
		fmt.Println(7, err)
	}

	// Output:
	//1 want function to define slot type but got type "int"
	//3 want slot type "func(int)" but got type "func(float64)"
	//5 want 1 arguments but got 0
	//6 want type "int" in argument 0 but got type "string"
	//rightFunc(1)
}
