package example

import (
	"fmt"
	"github.com/carylorrk/gosig"
)

func helloWorld() {
	fmt.Println("Hello, World!")
}

func ExampleHelloWorld() {
	sig, _ := gosig.NewSignal(func() {})
	sig.Connect(helloWorld)
	sig.Emit()
	// Output:
	// Hello, World!
}
