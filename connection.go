package gosig

type Connection struct {
	signal  *Signal
	handler SlotHandler
}

func newConnection(signal *Signal, handler SlotHandler) *Connection {
	return &Connection{signal, handler}
}

func (self *Connection) Disconnect() error {
	return self.signal.Disconnect(self.handler)
}

func (self *Connection) Block() error {
	return self.signal.Block(self.handler)
}

func (self *Connection) Unblock() error {
	return self.signal.Unblock(self.handler)
}
